# Postgres Backup Scaduler

A Helm chart for Backup PostgresSQL to S3 (supports periodic backups)

based on: [https://github.com/schickling/dockerfiles/tree/master/postgres-backup-s3#automatic-periodic-backups](https://github.com/schickling/dockerfiles/tree/master/postgres-backup-s3#automatic-periodic-backups)

